/**
 * Created by Vinicius on 6/16/15.
 */
public class Produto {
    String descricao;
    int valorMercado;

    public Produto(String descricao, int valorMercado) {
        this.descricao    = descricao;
        this.valorMercado = valorMercado;
    }

    public int getValorMercado() {
        return valorMercado;
    }

    public void setValorMercado(int valorMercado) {
        this.valorMercado = valorMercado;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
