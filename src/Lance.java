/**
 * Created by Vinicius on 6/16/15.
 */
public class Lance {
    int autor;
    int valor;

    public Lance(int autor, int valor) {
        this.autor = autor;
        this.valor = valor;
    }

    public void exibeValor() {
        System.out.println(this.valor);
    }

    public int getAutor() {
        return autor;
    }

    public void setAutor(int autor) {
        this.autor = autor;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
}
