import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Vinicius on 6/16/15.
 */
public class LancePremiado extends Lance {
    List<Integer> listaPremiados = new ArrayList<Integer>();

    public LancePremiado(int autor, int valor) {
        super(autor, valor);

        this.geraLancesPremiados();
    }

    public boolean estaPremiado(int numeroLance) {
        return this.listaPremiados.contains(numeroLance);
    }

    public List<Integer> getListaPremiados() {
        return listaPremiados;
    }

    public boolean recebePalpite() {
        Scanner entrada = new Scanner(System.in);

        int palpite;

         do {
             System.out.println("VOCE FOI PREMIADO! FACA UM PALPITE DE 1 A 10");
             palpite = entrada.nextInt();
         } while (palpite <= 0 || palpite > 10);

        return this.estaPremiado(palpite);
    }

    private void geraLancesPremiados() {
        while (this.listaPremiados.size() < 6) {
            int lanceRandomico = (int)(Math.random() * 10);

            if ( !this.listaPremiados.contains(lanceRandomico) && lanceRandomico != 0 ){
                this.listaPremiados.add(lanceRandomico);
            }
        }
    }
}
