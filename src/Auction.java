/**
 * Created by Vinicius on 6/16/15.
 */

import javafx.application.Application;
import javafx.stage.Stage;

public class Auction extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Produto produto = new Produto("MotoG - 2ª geração", 800);

        Leilao leilao = new Leilao(produto, 10);
        leilao.realizaLeilao();
    }
}
