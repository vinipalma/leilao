import java.util.Scanner;

/**
 * Created by Vinicius on 6/16/15.
 */
public class Leilao {
    private Lance[] lances;
    private Produto produto;
    private int quantidadeMaximaLances;
    private int ultimoLance;

    private boolean usuarioJogou = false;

    public Leilao(Produto produto, int quantidadeMaximaLances) {
        this.produto = produto;
        this.quantidadeMaximaLances = quantidadeMaximaLances;
        this.ultimoLance = 0;

        this.lances = new Lance[this.quantidadeMaximaLances];
    }

    public void realizaLeilao() {
        Scanner entrada = new Scanner(System.in);

        for (int i = 0; i < this.lances.length; i++) {
            System.out.println("Lance #" + (i + 1));

            if (!this.usuarioJogou) {
                int valor = 0;
                System.out.println("Sua vez de fazer um lance!");

                while (valor <= ultimoLance) {

                    if (this.ultimoLance == 0) {
                        System.out.println("Digite o primeiro lance:");
                    }
                    else {
                        System.out.println("Digite um lance, (deve ser maior que): " + this.ultimoLance);
                    }

                    valor = entrada.nextInt();
                }

                Lance lance = new Lance(2, valor);

                LancePremiado lancePremiado = new LancePremiado(lance.getAutor(), lance.getValor());

                if (lancePremiado.recebePalpite()) {
                    System.out.println("VOCE GANHOU \n \n");
                    return;
                }

                else {
                    System.out.println("VOCE NAO GANHOU \n \n");
                }

                this.lances[i] = lance;
                this.usuarioJogou = true;
                this.ultimoLance = valor;
            }

            else {
                System.out.println("Player" + i + " fez um lance de: " + (ultimoLance + 1));
                Lance lance = new Lance(1, 1);
                this.lances[i] = lance;
                this.usuarioJogou = false;
                this.ultimoLance += 1;
            }
        }
    }
}
